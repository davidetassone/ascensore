//Creazione PIN Arduino
int sensore0 = 9, sensore1 = 2, sensore2 = 3, sensore3 = 4;                                            //Sensori
int chiamata0 = 5, chiamata1 = 6, chiamata2 = 7, chiamata3 = 8;                                        //Pulsantiera chiamate
int motoreCabinaSalita = 10, motoreCabinaDiscesa = 11, motorePortaAperta = 13, motorePortaChiusa = 12; //Controllo motori

//Stato sensori
int statoSensore0 = 0, statoSensore1 = 0, statoSensore2 = 0, statoSensore3 = 0;

//Stato chiamate
int statoChiamata0 = 0, statoChiamata1 = 0, statoChiamata2 = 0, statoChiamata3 = 0;

unsigned long tempoCorrente;

//Setup Arduino
void setup()
{
  Serial.begin(9600);

  //Settaggio I-O dei PIN
  pinMode(sensore0, INPUT);
  pinMode(sensore1, INPUT);
  pinMode(sensore2, INPUT);
  pinMode(sensore3, INPUT);

  pinMode(chiamata0, INPUT);
  pinMode(chiamata1, INPUT);
  pinMode(chiamata2, INPUT);
  pinMode(chiamata3, INPUT);

  pinMode(motoreCabinaSalita, OUTPUT);
  pinMode(motoreCabinaDiscesa, OUTPUT);
  pinMode(motorePortaAperta, OUTPUT);
  pinMode(motorePortaChiusa, OUTPUT);

  //Inizializzazione PIN a spenti
  digitalWrite(sensore0, LOW);
  digitalWrite(sensore1, LOW);
  digitalWrite(sensore2, LOW);
  digitalWrite(sensore3, LOW);

  digitalWrite(chiamata0, LOW);
  digitalWrite(chiamata1, LOW);
  digitalWrite(chiamata2, LOW);
  digitalWrite(chiamata3, LOW);

  digitalWrite(motoreCabinaSalita, LOW);
  digitalWrite(motoreCabinaDiscesa, LOW);
  digitalWrite(motorePortaAperta, LOW);
  digitalWrite(motorePortaChiusa, LOW);
}

//Procedure Chiamate
void ChiamateAggiornamento()
{
  statoChiamata0 = digitalRead(chiamata0);
  statoChiamata1 = digitalRead(chiamata1);
  statoChiamata2 = digitalRead(chiamata2);
  statoChiamata3 = digitalRead(chiamata3);
}

void Chiamata0Aggiornamento()
{
  statoChiamata0 = digitalRead(chiamata0);
}

void Chiamata1Aggiornamento()
{
  statoChiamata1 = digitalRead(chiamata1);
}

void Chiamata2Aggiornamento()
{
  statoChiamata2 = digitalRead(chiamata2);
}

void Chiamata3Aggiornamento()
{
  statoChiamata3 = digitalRead(chiamata3);
}

void ChiamateReset()
{
  statoChiamata0 = 0;
  statoChiamata1 = 0;
  statoChiamata2 = 0;
  statoChiamata3 = 0;
}

//Procedure Sensori
void SensoriAggiornamento()
{
  statoSensore0 = digitalRead(sensore0);
  statoSensore1 = digitalRead(sensore1);
  statoSensore2 = digitalRead(sensore2);
  statoSensore3 = digitalRead(sensore3);
}

void Sensore0Aggiornamento()
{
  statoSensore0 = digitalRead(sensore0);
}

void Sensore1Aggiornamento()
{
  statoSensore1 = digitalRead(sensore1);
}

void Sensore2Aggiornamento()
{
  statoSensore2 = digitalRead(sensore2);
}

void Sensore3Aggiornamento()
{
  statoSensore3 = digitalRead(sensore3);
}

void SensoriReset()
{
  statoSensore0 = 0;
  statoSensore1 = 0;
  statoSensore2 = 0;
  statoSensore3 = 0;
}

bool SensoriIsOff()
{
  bool stato;
  if (statoSensore0 == 0 && statoSensore1 == 0 && statoSensore2 == 0 && statoSensore3)
  {
    stato = true;
  }
  else
  {
    stato = false;
  }

  return stato;
}

int SensoreStato(int numeroPiano)
{ //Dato il numero di un piano individua il relativo sensore
  if (numeroPiano == 0)
  {
    return statoSensore0;
  }
  if (numeroPiano == 1)
  {
    return statoSensore1;
  }
  if (numeroPiano == 2)
  {
    return statoSensore2;
  }
  if (numeroPiano == 3)
  {
    return statoSensore3;
  }

  return 90;
}

int SensoreAttivo()
{
  SensoriAggiornamento();

  //Cerco tra i sensori quello attivo e restituisco il suo numero. In caso ce ne siano piu di uno restituisco 100 come errore
  int numeroSensoriAttivi; //Conto quanti sensori attivi ci sono
  int sensoreAttivo;       //Numero del piano in cui e' attivo il sensore

  for (int i = 0; i < 4; i++)
  {
    if (SensoreStato(i) == HIGH)
    {
      sensoreAttivo = i;
      numeroSensoriAttivi++;
    }
  }

  //Serial.print(numeroSensoriAttivi);

  if (numeroSensoriAttivi == 1)
  {
    return sensoreAttivo;
  }
  else
  {
    if (numeroSensoriAttivi == 0)
    {
      return -1;
    }
    else
    {
      return 100;
    }
  }
}

//Procedure motori
void CabinaSalita()
{
  digitalWrite(motoreCabinaDiscesa, LOW);
  digitalWrite(motoreCabinaSalita, HIGH);
}

void CabinaDiscesa()
{
  digitalWrite(motoreCabinaSalita, LOW);
  digitalWrite(motoreCabinaDiscesa, HIGH);
}

void CabinaSTOP()
{
  digitalWrite(motoreCabinaSalita, LOW);
  digitalWrite(motoreCabinaDiscesa, LOW);
}

void PortaApri()
{
  digitalWrite(motorePortaChiusa, LOW);
  digitalWrite(motorePortaAperta, HIGH);
}

void PortaChiudi()
{
  digitalWrite(motorePortaAperta, LOW);
  digitalWrite(motorePortaChiusa, HIGH);
}

void PortaSTOP()
{
  digitalWrite(motorePortaAperta, LOW);
  digitalWrite(motorePortaChiusa, LOW);
}

//Algoritmo
void VaiAlPiano(int pianoDestinazione)
{
  int sensoreAttivo;
  int sensoreAttivoAggiornato;
  SensoriAggiornamento();

  Serial.println("Sensore 0: ");
  Serial.println(statoSensore0);
  Serial.println("Sensore 1: ");
  Serial.println(statoSensore1);
  Serial.println("Sensore 2: ");
  Serial.println(statoSensore2);
  Serial.println("Sensore 3: ");
  Serial.println(statoSensore3);
  Serial.println("\n\n");

  while ((SensoreAttivo() != pianoDestinazione) || (SensoreAttivo() != -1))
  {
    Serial.println("Sensore 0: ");
    Serial.println(statoSensore0);
    Serial.println("Sensore 1: ");
    Serial.println(statoSensore1);
    Serial.println("Sensore 2: ");
    Serial.println(statoSensore2);
    Serial.println("Sensore 3: ");
    Serial.println(statoSensore3);
    Serial.println("\n\n");

    SensoriAggiornamento();

    sensoreAttivoAggiornato = SensoreAttivo();
    if (SensoreAttivo() != -1)
    {
      sensoreAttivo = sensoreAttivoAggiornato;
    }

    if (sensoreAttivo > pianoDestinazione)
    {
      //Il piano di destinazione e' sopra al piano corrente
      Serial.println("SOPRA");
      CabinaSalita();
    }
    else
    {
      //Il piano di destinazione e' sotto al piano corrente
      Serial.println("SOTTO");
      CabinaDiscesa();
    }
  }

  CabinaSTOP(); //Sono arrivato al piano, fermo tutto

  Serial.println("------FINE------");
  Serial.println("Sensore 0: ");
  Serial.println(statoSensore0);
  Serial.println("Sensore 1: ");
  Serial.println(statoSensore1);
  Serial.println("Sensore 2: ");
  Serial.println(statoSensore2);
  Serial.println("Sensore 3: ");
  Serial.println(statoSensore3);
  Serial.println("\n\n");

  //Gestione porte
  PortaApri();
  delay(1800);
  PortaChiudi();
  delay(1800);
  PortaSTOP();
}

void loop()
{
  ChiamateAggiornamento();
  SensoriAggiornamento();

  if (statoChiamata0 == HIGH)
  {
    VaiAlPiano(0);
  }

  if (statoChiamata1 == HIGH)
  {
    VaiAlPiano(1);
  }

  if (statoChiamata2 == HIGH)
  {
    VaiAlPiano(2);
  }

  if (statoChiamata3 == HIGH)
  {
    VaiAlPiano(3);
  }
}